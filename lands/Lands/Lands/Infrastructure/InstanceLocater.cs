﻿namespace Lands.Infrastructure
{
    using ViewModels;
    public class InstanceLocater
    {
        #region Properties
        public MainViewModel Main
        {
            get;
            set;
        }
        #endregion

        #region Constructors
        public InstanceLocater()
        {
            this.Main = new MainViewModel();
        }
        #endregion
    }
}
